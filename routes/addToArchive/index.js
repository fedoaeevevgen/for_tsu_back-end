var moment = require('moment');
const Ad = require('./../../db/Schma');

module.exports = async function(app){
    var cronDate = moment().add(30, 'day');
    // Если найдена запись, страше cron_date, с type: 'lost'      
    Ad.updateMany({cron_date : {$lte : new Date()}, type: 'lost'}, {'$set': {type: 'archive', cron_date: cronDate}}, {upsert: false}, (err, item) => {
        if (err) 
            return console.log(err)
        else if (item.nModified > 0)   
            console.log("Успешно обновлено " + item.nModified + " сообщений");
    });
    // Если запись в архиве больше месяца
    Ad.deleteMany({cron_date : {$lte : new Date()}, type: 'archive'})
        .then((result) => {
            if (result.n > 0) 
                console.log("Успешно удалено " + result.n + " сообщений");
        });
};