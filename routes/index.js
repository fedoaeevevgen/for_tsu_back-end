
const fs = require('fs');
const mongoose = require('mongoose');
const user = require('./../db/Schma');

module.exports = async function(app){  
    app.post('/add', require('./add'));
    app.post('/edit', require('./edit'));
    app.post('/editPhone', require('./editPhone'));
    app.post('/adminEdit', require('./adminEdit'));
    app.post('/adminApprove', require('./adminApprove'));
    app.post('/addForPhone', require('./addForPhone'));
    app.post('/del',require('./del'));
    app.post('/archive',require('./archive'));
    app.get('/search', require('./search'));
    app.get('/all',require('./all'));
    app.get('/allTrue',require('./allTrue'));
    app.get('/allFalse',require('./allFalse'));  
    app.get('/BackAllTrue',require('./BackAllTrue'));
    app.get('/findById',require('./findById'));
    app.get('/lastWeek',require('./lastWeek'));
};